# SkunkWorks: An open-source NCAA football ranking and prediction program.
# Copyright (C) 2021  Arthur Rajala

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Add the root package directory to path for importing
# This is so user does not need to run setup.py or modify PYTHONPATH
from os.path import dirname, join, realpath
import sys
root = dirname(dirname(realpath(__file__)))
sys.path.append(root)
sys.path.append(join(dirname(root), "TheKickIsBAD"))

# Standard imports
import argparse
import time
import torch
from the_kick_is_bad import utils
from tqdm import tqdm
from torch.optim import AdamW
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.utils.data.dataloader import DataLoader

# SkunkWorks imports
from datasets.sequence_dataset import SequenceDataset
from models.temporal_model import TemporalModel


# Set CUDA context
use_cuda = torch.cuda.is_available()
device = torch.device("cuda:0" if use_cuda else "cpu")
torch.backends.cudnn.benchmark = True

def load_data(dataset_name, batch_size, sequence_length):
    
    train_dataset = SequenceDataset(dataset_name, "training", batch_size, sequence_length)
    train_loader = DataLoader(train_dataset,
                              batch_size=batch_size,
                              shuffle=True,
                              num_workers=4,
                              persistent_workers=True,
                              pin_memory=False,
                              drop_last=True)
    
    test_dataset = SequenceDataset(dataset_name, "testing", batch_size, sequence_length)
    test_loader = DataLoader(test_dataset,
                             batch_size=batch_size,
                             shuffle=True,
                             num_workers=4,
                             persistent_workers=True,
                             pin_memory=False,
                             drop_last=True)

    return train_loader, test_loader

def train_model(model, name, train_loader, valid_loader, patience=8):

    absolute_path = utils.get_abs_path(__file__)
    model_filename = f"{absolute_path}/checkpoints/{name}.pt"

    criterion = torch.nn.BCELoss()
    optimizer = AdamW(model.parameters(), lr=0.01)
    scheduler = ReduceLROnPlateau(optimizer,
                                  mode="min",
                                  patience=patience,
                                  min_lr=0.000001,
                                  verbose=True)

    best_train_loss = 1e9
    best_valid_loss = 1e9
    best_train_acc = 0
    best_valid_acc = 0
    no_improvement_count = 0

    epoch = 1
    while True:

        start_time = time.perf_counter()

        # Train
        model.train()
        train_loss = 0
        train_correct = 0
        train_total = 0
        for batch_seq_data, batch_stat_data, batch_labels in tqdm(train_loader):

            # Move the batch to the GPU
            batch_seq_data = batch_seq_data.to(device, non_blocking=True)
            batch_stat_data = batch_stat_data.to(device, non_blocking=True)
            batch_labels = batch_labels.to(device, non_blocking=True)
            
            # Clear gradients w.r.t. parameters
            optimizer.zero_grad(set_to_none=True)

            # Run the forward pass of the model
            outputs = model(batch_seq_data, batch_stat_data, reset=True)
            loss = criterion(outputs, batch_labels)

            # Run the backward pass and scale gradients
            loss.backward()
            optimizer.step()

            # Update metrics
            train_loss += loss.item()
            train_correct += (torch.round(outputs) == batch_labels).sum().item()
            train_total += batch_labels.size(0)

        # Calculate final metrics
        train_loss /= len(train_loader)
        train_accuracy = 100 * train_correct / train_total

        # Validate
        model.eval()
        with torch.no_grad():

            valid_loss = 0
            valid_correct = 0
            valid_total = 0
            for batch_seq_data, batch_stat_data, batch_labels in tqdm(valid_loader):

                # Move the batch to the GPU
                batch_seq_data = batch_seq_data.to(device, non_blocking=True)
                batch_stat_data = batch_stat_data.to(device, non_blocking=True)
                batch_labels = batch_labels.to(device, non_blocking=True)

                # Run the forward pass of the model
                outputs = model(batch_seq_data, batch_stat_data)
                loss = criterion(outputs, batch_labels)

                # Update metrics
                valid_loss += loss.item()
                valid_correct += (torch.round(outputs) == batch_labels).sum().item()
                valid_total += batch_labels.size(0)

            # Calculate final metrics
            valid_loss /= len(valid_loader)
            valid_accuracy = 100 * valid_correct / valid_total

            scheduler.step(valid_loss)

            # Check if this is the best validation result
            if valid_loss < best_valid_loss:
                best_train_loss = train_loss
                best_valid_loss = valid_loss
                best_train_acc = train_accuracy
                best_valid_acc = valid_accuracy
                no_improvement_count = 0
                torch.save(model.state_dict(), model_filename)
            else:
                no_improvement_count += 1
                if no_improvement_count > (patience * 2):
                    print("Early stopping condition met")
                    break

        stop_time = time.perf_counter()
        print(f"Epoch {epoch}: time={stop_time-start_time:.1f} s, lr={scheduler._last_lr[0]}")
        print(f"\ttrain loss={train_loss:.6f}, train acc={train_accuracy:.2f}%")
        print(f"\tvalid loss={valid_loss:.6f}, valid acc={valid_accuracy:.2f}%")
        
        epoch += 1

    metrics = {
        "train loss": best_train_loss,
        "train accuracy": best_train_acc,
        "valid loss": best_valid_loss,
        "valid accuracy": best_valid_acc
    }
    metrics_filename = f"{absolute_path}/checkpoints/{name}_metrics.json"
    utils.write_json(metrics, metrics_filename)

def parse_args():

    parser = argparse.ArgumentParser(description="Train")

    parser.add_argument("--model_name", type=str, required=True)
    parser.add_argument("--dataset_name", type=str, required=True)
    parser.add_argument("--batch_size", type=int, required=False, default=32)
    parser.add_argument("--sequence_length", type=int, required=False, default=2)
    parser.add_argument("--num_lstm_layers", type=int, required=False, default=1)

    return parser.parse_args()


if __name__ == "__main__":

    args = parse_args()

    # Load the dataset metadata
    train_loader, test_loader = load_data(args.dataset_name, args.batch_size, args.sequence_length)

    # Save the model config to read after training
    model_config = {
        "lstm_input_dim": 28,
        "lstm_hidden_dim": 16,
        "num_lstm_layers": args.num_lstm_layers,
        "fc_input_add_dim": 12,
        "fc_output_dim": 1,
        "sequence_length": args.sequence_length,
    }
    absolute_path = utils.get_abs_path(__file__)
    config_filename = f"{absolute_path}/checkpoints/{args.model_name}.json"
    utils.write_json(model_config, config_filename)

    # Create the model
    model = TemporalModel(model_config, args.batch_size, device).to(device, non_blocking=True)
    print(f"Trainable model parameters: {model.count_parameters()}")

    # Train the model
    start_time = time.perf_counter()
    train_model(model, args.model_name, train_loader, test_loader, 4)
    stop_time = time.perf_counter()
    print(f"Total training time: {stop_time - start_time:.1f} s")
