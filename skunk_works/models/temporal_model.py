# SkunkWorks: An open-source NCAA football ranking and prediction program.
# Copyright (C) 2021  Arthur Rajala

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import torch
from torch import nn

class TemporalModel(nn.Module):

    def __init__(self, config, batch_size, device):

        super().__init__()

        self.lstm_input_dim = config["lstm_input_dim"]
        self.lstm_hidden_dim = config["lstm_hidden_dim"]
        self.num_lstm_layers = config["num_lstm_layers"]

        if self.num_lstm_layers > 1:
            self.dropout = 0.2
        else:
            self.dropout = 0
        self.lstm = nn.LSTM(input_size=self.lstm_input_dim,
                            hidden_size=self.lstm_hidden_dim,
                            num_layers=self.num_lstm_layers,
                            dropout=self.dropout,
                            batch_first=True)

        self.fc_input_dim = self.lstm_hidden_dim + config["fc_input_add_dim"]
        self.fc_output_dim = config["fc_output_dim"]

        self.fc = nn.Linear(in_features=self.fc_input_dim,
                            out_features=self.fc_output_dim)

        self.fc_act = nn.Sigmoid()

        self.sequence_length = config["sequence_length"]
        self.batch_size = batch_size
        self.device = device

        self.init_state()

    def init_state(self):

        self.h = torch.zeros(self.num_lstm_layers, self.batch_size, self.lstm_hidden_dim).to(self.device, non_blocking=True)
        self.c = torch.zeros(self.num_lstm_layers, self.batch_size, self.lstm_hidden_dim).to(self.device, non_blocking=True)

    def forward(self, x_seq, x_stat, reset=True):

        if reset:
            self.init_state()

        # Pass game-by-game sequence through the temporal layers
        x_seq, (self.h, self.c) = self.lstm(x_seq, (self.h, self.c))

        # Append static features to last sequence of temporal output
        x_seq = x_seq[:, -1, :]
        x = torch.cat((x_seq, x_stat), 1)

        # Pass final tensor through dense layer
        x = self.fc(x)
        x = self.fc_act(x)
        
        return x

    def count_parameters(self, requires_grad_only=False):

        if requires_grad_only:
            return sum(p.numel() for p in self.parameters() if p.requires_grad)
        else:
            return sum(p.numel() for p in self.parameters())
