# SkunkWorks: An open-source NCAA football ranking and prediction program.
# Copyright (C) 2021  Arthur Rajala

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Add the root package directory to path for importing
# This is so user does not need to run setup.py or modify PYTHONPATH
from os.path import dirname, join, realpath
import sys
root = dirname(dirname(realpath(__file__)))
sys.path.append(root)
sys.path.append(join(dirname(root), "TheKickIsBAD"))

# Standard imports
import torch
from the_kick_is_bad import utils

# SkunkWorks imports
from models.temporal_model import TemporalModel


def read_model(name, device, batch_size=1):

    # Read the model config
    absolute_path = utils.get_abs_path(__file__)
    config_filename = f"{absolute_path}/checkpoints/{name}.json"
    config = utils.read_json(config_filename)

    model_filename = f"{absolute_path}/checkpoints/{name}.pt"
    model = TemporalModel(config, batch_size, device)
    model.load_state_dict(torch.load(model_filename))

    return model.to(device, non_blocking=True)
