# SkunkWorks: An open-source NCAA football ranking and prediction program.
# Copyright (C) 2019  Arthur Rajala

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Add the root package directory to path for importing
# This is so user does not need to run setup.py or modify PYTHONPATH
from os.path import dirname, join, realpath
import sys
from typing import Sequence
root = dirname(dirname(realpath(__file__)))
sys.path.append(join(root, "TheKickIsBAD"))

# Standard includes
import numpy as np
import the_kick_is_bad
import time
import torch
from torch.utils.data.dataloader import DataLoader
from the_kick_is_bad import utils

# SkunkWorks imports
from datasets.sequence_dataset import SequenceDataset
from models.read_model import read_model

# Set CUDA context
use_cuda = torch.cuda.is_available()
device = torch.device("cuda:0" if use_cuda else "cpu")
torch.backends.cudnn.benchmark = True


def predict(year, week, model):

    scores = the_kick_is_bad.read_scores(year, week)

    # Check if the week is 'bowl' week
    num_weeks = the_kick_is_bad.read_number_of_weeks(year)
    week, _ = utils.check_week(week, num_weeks)

    # Loop through scores to make predictions
    samples = []
    for game in scores["games"]:

        # Get the team names
        away_team = game["game"]["away"]["names"]["standard"]
        home_team = game["game"]["home"]["names"]["standard"]

        # Create the dataset
        samples.append([str(year), str(week), away_team, home_team, "0", "0", "0"])

    dataset = SequenceDataset(samples, "predicting", 1, model.sequence_length, from_file=False)
    loader = DataLoader(dataset, batch_size=1, shuffle=False, num_workers=4, persistent_workers=True, pin_memory=False, drop_last=False)

    # Run prediction model
    scores = []
    outputs = []
    model.eval()
    with torch.no_grad():
        for seq_data, stat_data, _ in loader:
            seq_data = seq_data.to(device, non_blocking=True)
            stat_data = stat_data.to(device, non_blocking=True)
            raw = model(seq_data, stat_data)
            scores.append(raw.item())
            outputs.append(torch.round(raw).item())

    # Analyze predictions
    predictions = []
    for i in range(0, len(samples)):
        sample = samples[i]
        away_team = sample[2]
        home_team = sample[3]
        if outputs[i] == 1:
            predicted_winner = home_team
            prediction_confidence = scores[i]
        else:
            predicted_winner = away_team
            prediction_confidence = 1 - scores[i]

        # Pack predictions
        predictions.append({
            "away team": away_team,
            "home team": home_team,
            "predicted winner": predicted_winner,
            "prediction confidence": prediction_confidence
        })
    
    # Sort predictions by prediction confidence
    predictions = sorted(predictions, key=lambda p: p["prediction confidence"], reverse=True)

    # Print predictions
    predictions_file_string = "AwayTeam,HomeTeam,PredictedWinner,Confidence\n"
    for prediction in predictions:

        # Print to console in pretty format
        print("{0} @ {1}: Predicted Winner: {2}, Confidence: {3:.3f}".format(
            prediction["away team"], prediction["home team"], prediction["predicted winner"], prediction["prediction confidence"]))

        # Print to file string in csv format
        predictions_file_string += "{0},{1},{2},{3:.3f}\n".format(
            prediction["away team"], prediction["home team"], prediction["predicted winner"], prediction["prediction confidence"])
        
    # Create the predictions file with absolute path
    absolute_path = utils.get_abs_path(__file__)
    filename = f"{absolute_path}/predictions/{year}/predictions-{year}-{week:02}.csv"
    utils.write_string(predictions_file_string, filename)
    

if __name__ == "__main__":
    year = int(sys.argv[1])
    week = sys.argv[2]
    model_name = sys.argv[3]
    if week != "bowl":
        week = int(week)

    # Read the model from file
    model = read_model(model_name, device)

    # Make the predictions
    start_time = time.perf_counter()
    predict(year, week, model)
    stop_time = time.perf_counter()
    print(f"Total prediction time: {stop_time - start_time:.1f} s")
