# SkunkWorks: An open-source NCAA football ranking and prediction program.
# Copyright (C) 2019  Arthur Rajala

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Add the root package directory to path for importing
# This is so user does not need to run setup.py or modify PYTHONPATH
from os.path import dirname, join, realpath
import sys
root = dirname(dirname(realpath(__file__)))
sys.path.append(join(root, "TheKickIsBAD"))

# Standard imports
import math
import numpy as np
import statistics
import the_kick_is_bad
from the_kick_is_bad import utils

# SkunkWorks imports
from rankings.read_rankings import read_rankings


def rank(year, week):
    
    stats = the_kick_is_bad.read_stats(year, week)

    # Check if the week is "bowl" week
    num_weeks = the_kick_is_bad.read_number_of_weeks(year)
    week, _ = utils.check_week(week, num_weeks)
    
    teams, _ = the_kick_is_bad.read_teams(year)

    team_rankings = calculate_team_rankings(year, week, num_weeks, stats, teams)

    calculate_conference_rankings(year, week, teams, team_rankings)

    calculate_division_rankings(year, week, teams, team_rankings)

def calculate_team_rankings(year, week, num_weeks, stats, teams):

    strength_of_schedules = calculate_strength_of_schedules(stats, teams)

    # Read previous rankings for delta rank calculation
    if week > 1:
        prev_rankings = read_rankings(year, week - 1)

    rank_scores = initialize_rank_scores(teams)
    for read_week in range(1, week + 1):

        if read_week == num_weeks + 1:
            scores = the_kick_is_bad.read_scores(year, "bowl")
        else:
            scores = the_kick_is_bad.read_scores(year, read_week)

        # Calculate rank scores game by game
        for game in scores["games"]:
            if game["game"]["gameState"] != "final":
                continue

            # Get the team names
            away_team = game["game"]["away"]["names"]["standard"]
            home_team = game["game"]["home"]["names"]["standard"]

            # Get the game scores
            away_score = int(game["game"]["away"]["score"])
            home_score = int(game["game"]["home"]["score"])
            point_margin = abs(home_score - away_score)

            # Calculate the rank scores
            if away_score > home_score:
                away_rank_score = (1 - math.exp(-0.07 * point_margin)) * strength_of_schedules[away_team]["strength of schedule in wins"]
                home_rank_score = -(1 - math.exp(-0.07 * point_margin)) * (1 - strength_of_schedules[home_team]["strength of schedule in losses"])
            else:
                away_rank_score = -(1 - math.exp(-0.07 * point_margin)) * (1 - strength_of_schedules[away_team]["strength of schedule in losses"])
                home_rank_score = (1 - math.exp(-0.07 * point_margin)) * strength_of_schedules[home_team]["strength of schedule in wins"]

            rank_scores[away_team].append(away_rank_score)
            rank_scores[home_team].append(home_rank_score)

    # Normalize rank scores by number of games played
    for team in rank_scores:
        if rank_scores[team] == []:
            rank_scores[team] = 0
        else:
            rank_scores[team] = statistics.mean(rank_scores[team])

    # Convert to a list for indexed sorting
    rank_scores_list = []
    for team in teams:
        rank_scores_list.append(rank_scores[team])
    sort_indexes = np.argsort(-np.array(rank_scores_list))

    rankings = {}
    i = 0
    if week > 1:
        for team in teams:
            new_rank = np.where(sort_indexes == i)[0][0] + 1
            rankings[team] = {
                "rank": new_rank,
                "previous rank": prev_rankings[team]["rank"],
                "delta rank": prev_rankings[team]["rank"] - new_rank,
                "rank score": rank_scores_list[i],
            }
            i += 1
    else:
        for team in teams:
            new_rank = np.where(sort_indexes == i)[0][0] + 1
            rankings[team] = {
                "rank": new_rank,
                "previous rank": 0,
                "delta rank": 0 - new_rank,
                "rank score": rank_scores_list[i],
            }
            i += 1

    # Print team rankings to console
    print("Team Rankings:\n")
    sorted_rank_scores = sorted(rank_scores.items(), key=lambda x:(x[1], x[0]), reverse=True)
    for rank_score in sorted_rank_scores:
        print(f"{rank_score[0]}: {rank_score[1]:.3f}")

    # Print team rankings to file
    team_rankings_file_string = "Team,Rank,PrevRank,DeltaRank,RankScore\n"
    for team in rankings:

        # Print to file string in csv format
        team_rankings_file_string += "{0},{1:.0f},{2:.0f},{3:.0f},{4:.3f}\n".format(
            team, rankings[team]["rank"], rankings[team]["previous rank"], rankings[team]["delta rank"], rankings[team]["rank score"])

    # Create the rankings file with absolute path
    absolute_path = utils.get_abs_path(__file__)
    filename = f"{absolute_path}/rankings/{year}/team_rankings-{year}-{week:02}.csv"
    utils.write_string(team_rankings_file_string, filename)

    return rankings

def calculate_conference_rankings(year, week, teams, team_rankings):

    # Make lists of all the team scores in each conference
    conference_rankings = {}
    for team in teams:
        conference = teams[team]["conference"]
        if conference not in conference_rankings:
            conference_rankings[conference] = []
        conference_rankings[conference].append(team_rankings[team]["rank score"])

    # Convert dictionary to averaged list for sorting
    sorted_conference_rankings = []
    for conference in conference_rankings:
        sorted_conference_rankings.append({
            "conference": conference,
            "score": statistics.mean(conference_rankings[conference])
        })
    
    # Sort conference rankings by average team score
    sorted_conference_rankings = sorted(sorted_conference_rankings, key=lambda p: p["score"], reverse=True)

    # Print conference rankings
    print("\nConference Rankings:\n")
    rank = 1
    conference_rankings_file_string = "Conference,Score\n"
    for conference_ranking in sorted_conference_rankings:

        # Print to file string in csv format
        conference = conference_ranking["conference"]
        score = conference_ranking["score"]
        conference_rankings_file_string += f"{conference},{score:.3f}\n"

        # Print to console in pretty format
        print(f"{rank}: {conference}, Score: {score:.3f}")
        rank += 1

    # Create the conference rankings file with absolute path
    absolute_path = utils.get_abs_path(__file__)
    filename = f"{absolute_path}/rankings/{year}/conference_rankings-{year}-{week:02}.csv"
    utils.write_string(conference_rankings_file_string, filename)

def calculate_division_rankings(year, week, teams, team_rankings):

    # Make lists of all the team scores in each division
    division_rankings = {}
    for team in teams:
        division = teams[team]["division"]
        if division not in division_rankings:
            division_rankings[division] = []
        division_rankings[division].append(team_rankings[team]["rank score"])

    # Convert dictionary to averaged list for sorting
    sorted_division_rankings = []
    for division in division_rankings:
        sorted_division_rankings.append({
            "division": division,
            "score": statistics.mean(division_rankings[division])
        })
    
    # Sort division rankings by average team score
    sorted_division_rankings = sorted(sorted_division_rankings, key=lambda p: p["score"], reverse=True)

    # Print division rankings
    print("\nDivision Rankings:\n")
    rank = 1
    division_rankings_file_string = "Division,Score\n"
    for division_ranking in sorted_division_rankings:

        # Print to file string in csv format
        division = division_ranking["division"]
        score = division_ranking["score"]
        division_rankings_file_string += f"{division},{score:.3f}\n"

        # Print to console in pretty format
        print(f"{rank}: {division}, Score: {score:.3f}")
        rank += 1

    # Create the division rankings file with absolute path
    absolute_path = utils.get_abs_path(__file__)
    filename = f"{absolute_path}/rankings/{year}/division_rankings-{year}-{week:02}.csv"
    utils.write_string(division_rankings_file_string, filename)

def calculate_strength_of_schedules(stats, teams):

    strength_of_schedules = initialize_strength_of_schedules(teams)

    for team in stats:

        if team not in teams:
            strength_of_schedules[team] = {
                "strength of schedule": [],
                "strength of schedule in wins": [],
                "strength of schedule in losses": []
            }
            strength_of_schedules[team]["strength of schedule"].append(0)
            strength_of_schedules[team]["strength of schedule in wins"].append(0)
            strength_of_schedules[team]["strength of schedule in losses"].append(0)
        
        # Calculate strength of schedule by BCS formula (2/3) * opponents" record + (1/3) * opponents" opponents" record
        opp_w = stats[team]["record"]["opponent wins"]["season"]
        opp_l = stats[team]["record"]["opponent losses"]["season"]
        opp_opp_w = stats[team]["record"]["opponents opponent wins"]["season"]
        opp_opp_l = stats[team]["record"]["opponents opponent losses"]["season"]
        sos = ((2 * opp_w / max(1, opp_w + opp_l) + opp_opp_w / max(1, opp_opp_w + opp_opp_l)) / 3)

        # Calculate strength of schedule by BCS formula, but only from games that were won
        opp_w_in_wins = stats[team]["record"]["opponent wins"]["in wins"]
        opp_l_in_wins = stats[team]["record"]["opponent losses"]["in wins"]
        opp_opp_w_in_wins = stats[team]["record"]["opponents opponent wins"]["in wins"]
        opp_opp_l_in_wins = stats[team]["record"]["opponents opponent losses"]["in wins"]
        sos_in_wins = ((2 * opp_w_in_wins / max(1, opp_w_in_wins + opp_l_in_wins) + opp_opp_w_in_wins / max(1, opp_opp_w_in_wins + opp_opp_l_in_wins)) / 3)

        # Calculate strength of schedule by BCS formula, but only from games that were lost
        opp_w_in_losses = stats[team]["record"]["opponent wins"]["in losses"]
        opp_l_in_losses = stats[team]["record"]["opponent losses"]["in losses"]
        opp_opp_w_in_losses = stats[team]["record"]["opponents opponent wins"]["in losses"]
        opp_opp_l_in_losses = stats[team]["record"]["opponents opponent losses"]["in losses"]
        sos_in_losses = ((2 * opp_w_in_losses / max(1, opp_w_in_losses + opp_l_in_losses) + opp_opp_w_in_losses / max(1, opp_opp_w_in_losses + opp_opp_l_in_losses)) / 3)

        strength_of_schedules[team]["strength of schedule"].append(sos)
        strength_of_schedules[team]["strength of schedule in wins"].append(sos_in_wins)
        strength_of_schedules[team]["strength of schedule in losses"].append(sos_in_losses)

    # Normalize strength of schedules by number of games played
    for team in strength_of_schedules:
        strength_of_schedules[team]["strength of schedule"] = statistics.mean(strength_of_schedules[team]["strength of schedule"])
        strength_of_schedules[team]["strength of schedule in wins"] = statistics.mean(strength_of_schedules[team]["strength of schedule in wins"])
        strength_of_schedules[team]["strength of schedule in losses"] = statistics.mean(strength_of_schedules[team]["strength of schedule in losses"])
        
    return strength_of_schedules

def initialize_strength_of_schedules(teams):
    strength_of_schedules = {}
    for team in teams:
        strength_of_schedules[team] = {
            "strength of schedule": [],
            "strength of schedule in wins": [],
            "strength of schedule in losses": []
        }
    return strength_of_schedules

def initialize_rank_scores(teams):
    rank_scores = {}
    for team in teams:
        rank_scores[team] = []
    return rank_scores


if __name__ == "__main__":
    year = int(sys.argv[1])
    week = sys.argv[2]
    if week != "bowl":
        week = int(week)
    rank(year, week)
