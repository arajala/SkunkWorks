# SkunkWorks: An open-source NCAA football ranking and prediction program.
# Copyright (C) 2021  Arthur Rajala

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Add the root package directory to path for importing
# This is so user does not need to run setup.py or modify PYTHONPATH
from os.path import dirname, join, realpath
import sys
root = dirname(dirname(dirname(realpath(__file__))))
sys.path.append(join(root, "TheKickIsBAD"))

# Standard imports
import os
import random
import the_kick_is_bad
from the_kick_is_bad import utils


def create_sequence_dataset(start_year, end_year, dataset_name, train_fraction=0.7):

    train_data = []
    test_data = []

    # Load the training data
    for year in range(start_year, end_year + 1):

        num_weeks = the_kick_is_bad.read_number_of_weeks(year)
        for week in range(1, num_weeks + 1):

            print(f"Loading data from year {year}, week {week:02}...")

            scores = the_kick_is_bad.read_scores(year, week)
            for game in scores["games"]:

                # Get the team names
                away_team = game["game"]["away"]["names"]["standard"]
                home_team = game["game"]["home"]["names"]["standard"]

                # Check if this is a valid game
                if game["game"]["gameState"] != "final":
                    continue

                if away_team == "FCS" or home_team == "FCS":
                    continue

                # Get the actual results
                away_score = int(game["game"]["away"]["score"])
                home_score = int(game["game"]["home"]["score"])
                if away_score > home_score:
                    home_won = 0
                elif away_score < home_score:
                    home_won = 1
                else:
                    continue

                # Pack the data and labels into a CSV string
                data = f"{year},{week},{away_team},{home_team},{away_score},{home_score},{home_won}"

                # Flip coin for train set or test set
                if (random.random() < train_fraction): # and not is_fcs_game:
                    train_data.append(data)
                else:
                    test_data.append(data)

    print(f"Training set size: {len(train_data)}")
    print(f"Testing set size: {len(test_data)}")

    # Save training data to file
    absolute_path = utils.get_abs_path(__file__)
    train_data_filename = f"{absolute_path}/training/{dataset_name}.csv"
    os.makedirs(os.path.dirname(train_data_filename), exist_ok=True)
    with open(train_data_filename, "w") as file:
        for data in train_data:
            file.write(f"{data}\n")

    # Save testing data to file
    test_data_filename = f"{absolute_path}/testing/{dataset_name}.csv"
    os.makedirs(os.path.dirname(test_data_filename), exist_ok=True)
    with open(test_data_filename, "w") as file:
        for data in test_data:
            file.write(f"{data}\n")


if __name__ == "__main__":
    start_year = int(sys.argv[1])
    end_year = int(sys.argv[2])
    dataset_name = sys.argv[3]
    train_fraction = float(sys.argv[4])
    create_sequence_dataset(start_year, end_year, dataset_name, train_fraction)
