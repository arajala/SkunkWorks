# SkunkWorks: An open-source NCAA football ranking and prediction program.
# Copyright (C) 2021  Arthur Rajala

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Add the root package directory to path for importing
# This is so user does not need to run setup.py or modify PYTHONPATH
from os.path import dirname, join, realpath
import sys
root = dirname(dirname(realpath(__file__)))
sys.path.append(join(root, "TheKickIsBAD"))

# Standard includes
import csv
import numpy as np
import torch
import the_kick_is_bad
from the_kick_is_bad import utils

class SequenceDataset(torch.utils.data.Dataset):

    def __init__(self, name, type, batch_size, sequence_length, from_file=True):

        self.batch_size = batch_size
        self.sequence_length = sequence_length
        self.num_game_features = 14
        self.num_season_features = 6

        absolute_path = utils.get_abs_path(__file__)
        self.norms = utils.read_json(f"{absolute_path}/norms.json")

        if from_file:
            self.samples = []
            filename = f"{absolute_path}/{type}/{name}.csv"
            with open(filename, "r") as file:
                reader = csv.reader(file)
                for row in reader:
                    self.samples.append(row)
        else:
            self.samples = name

    def __len__(self):

        return len(self.samples)

    def normalize(self, stat, norms):

        # Subtract mean and divide by standard deviation
        return (stat - norms[0]) / norms[1]

    def __getitem__(self, index):

        # Get the metadata for this sample (game to be predicted/trained)
        sample = self.samples[index]
        year = int(sample[0])
        week = int(sample[1])
        away_team = sample[2]
        home_team = sample[3]
        away_score = int(sample[4])
        home_score = int(sample[5])
        home_won = int(sample[6])

        # Read sequential features
        if week <= 1:
            sequential_features = np.zeros((self.sequence_length, self.num_game_features * 2))
        else:
            prev_week = week - 1
            stats = the_kick_is_bad.read_stats(year, prev_week)

            all_features = []
            away_games_played = stats[away_team]["games played"]["season"]
            home_games_played = stats[home_team]["games played"]["season"]
            for seq_idx in range(0, self.sequence_length):

                # Get away features
                away_stat_idx = away_games_played - 1 - seq_idx
                if away_stat_idx >= 0:
                    away_features = []
                    away_features.append(stats[away_team]["schedule"]["week"][away_stat_idx])
                    away_features.append(stats[away_team]["schedule"]["home"][away_stat_idx])
                    away_features.append(self.normalize(stats[away_team]["points"]["total"]["gained"][away_stat_idx], self.norms["points"]))
                    away_features.append(self.normalize(stats[away_team]["points"]["total"]["allowed"][away_stat_idx], self.norms["points"]))
                    away_features.append(self.normalize(stats[away_team]["rushing"]["yards"]["gained"][away_stat_idx], self.norms["rushing yards"]))
                    away_features.append(self.normalize(stats[away_team]["rushing"]["yards"]["allowed"][away_stat_idx], self.norms["rushing yards"]))
                    away_features.append(self.normalize(stats[away_team]["passing"]["yards"]["gained"][away_stat_idx], self.norms["passing yards"]))
                    away_features.append(self.normalize(stats[away_team]["passing"]["yards"]["allowed"][away_stat_idx], self.norms["passing yards"]))
                    away_features.append(self.normalize(stats[away_team]["penalties"]["number"]["gained"][away_stat_idx], self.norms["penalties"]))
                    away_features.append(self.normalize(stats[away_team]["penalties"]["number"]["allowed"][away_stat_idx], self.norms["penalties"]))
                    away_features.append(self.normalize(stats[away_team]["fumbles"]["total"]["gained"][away_stat_idx], self.norms["total fumbles"]))
                    away_features.append(self.normalize(stats[away_team]["fumbles"]["total"]["allowed"][away_stat_idx], self.norms["total fumbles"]))
                    away_features.append(self.normalize(stats[away_team]["interception returns"]["number"]["gained"][away_stat_idx], self.norms["interception returns"]))
                    away_features.append(self.normalize(stats[away_team]["interception returns"]["number"]["allowed"][away_stat_idx], self.norms["interception returns"]))
                else:
                    away_features = [0] * self.num_game_features

                # Get home features
                home_stat_idx = home_games_played - 1 - seq_idx
                if home_stat_idx >= 0:
                    home_features = []
                    home_features.append(stats[home_team]["schedule"]["week"][home_stat_idx])
                    home_features.append(stats[home_team]["schedule"]["home"][home_stat_idx])
                    home_features.append(self.normalize(stats[home_team]["points"]["total"]["gained"][home_stat_idx], self.norms["points"]))
                    home_features.append(self.normalize(stats[home_team]["points"]["total"]["allowed"][home_stat_idx], self.norms["points"]))
                    home_features.append(self.normalize(stats[home_team]["rushing"]["yards"]["gained"][home_stat_idx], self.norms["rushing yards"]))
                    home_features.append(self.normalize(stats[home_team]["rushing"]["yards"]["allowed"][home_stat_idx], self.norms["rushing yards"]))
                    home_features.append(self.normalize(stats[home_team]["passing"]["yards"]["gained"][home_stat_idx], self.norms["passing yards"]))
                    home_features.append(self.normalize(stats[home_team]["passing"]["yards"]["allowed"][home_stat_idx], self.norms["passing yards"]))
                    home_features.append(self.normalize(stats[home_team]["penalties"]["number"]["gained"][home_stat_idx], self.norms["penalties"]))
                    home_features.append(self.normalize(stats[home_team]["penalties"]["number"]["allowed"][home_stat_idx], self.norms["penalties"]))
                    home_features.append(self.normalize(stats[home_team]["fumbles"]["total"]["gained"][home_stat_idx], self.norms["total fumbles"]))
                    home_features.append(self.normalize(stats[home_team]["fumbles"]["total"]["allowed"][home_stat_idx], self.norms["total fumbles"]))
                    home_features.append(self.normalize(stats[home_team]["interception returns"]["number"]["gained"][home_stat_idx], self.norms["interception returns"]))
                    home_features.append(self.normalize(stats[home_team]["interception returns"]["number"]["allowed"][home_stat_idx], self.norms["interception returns"]))
                else:
                    home_features = [0] * self.num_game_features

                features = away_features + home_features
                all_features.append(features)

            all_features.reverse()
            sequential_features = np.array(all_features)

        sequential_data = torch.from_numpy(sequential_features).float()

        # Read season data
        prev_year = year - 1
        stats = the_kick_is_bad.read_stats(prev_year, "bowl")
        
        if away_team in stats:
            away_games_played = max(1, stats[away_team]["games played"]["season"])
            away_features = []
            away_features.append(self.normalize(sum(stats[away_team]["points"]["total"]["gained"]) / away_games_played, self.norms["points"]))
            away_features.append(self.normalize(sum(stats[away_team]["points"]["total"]["allowed"]) / away_games_played, self.norms["points"]))
            away_features.append(self.normalize(sum(stats[away_team]["rushing"]["yards"]["gained"]) / away_games_played, self.norms["rushing yards"]))
            away_features.append(self.normalize(sum(stats[away_team]["rushing"]["yards"]["allowed"]) / away_games_played, self.norms["rushing yards"]))
            away_features.append(self.normalize(sum(stats[away_team]["passing"]["yards"]["gained"]) / away_games_played, self.norms["passing yards"]))
            away_features.append(self.normalize(sum(stats[away_team]["passing"]["yards"]["allowed"]) / away_games_played, self.norms["passing yards"]))
        else:
            away_features = [0] * self.num_season_features
        if home_team in stats:
            home_games_played = max(1, stats[home_team]["games played"]["season"])
            home_features = []
            home_features.append(self.normalize(sum(stats[home_team]["points"]["total"]["gained"]) / home_games_played, self.norms["points"]))
            home_features.append(self.normalize(sum(stats[home_team]["points"]["total"]["allowed"]) / home_games_played, self.norms["points"]))
            home_features.append(self.normalize(sum(stats[home_team]["rushing"]["yards"]["gained"]) / home_games_played, self.norms["rushing yards"]))
            home_features.append(self.normalize(sum(stats[home_team]["rushing"]["yards"]["allowed"]) / home_games_played, self.norms["rushing yards"]))
            home_features.append(self.normalize(sum(stats[home_team]["passing"]["yards"]["gained"]) / home_games_played, self.norms["passing yards"]))
            home_features.append(self.normalize(sum(stats[home_team]["passing"]["yards"]["allowed"]) / home_games_played, self.norms["passing yards"]))
        else:
            home_features = [0] * self.num_season_features
        features = away_features + home_features
        season_features = np.array(features)
        season_data = torch.from_numpy(season_features).float()

        # Read label
        list_label = [home_won]
        np_label = np.array(list_label)
        label = torch.from_numpy(np_label).float()

        return sequential_data, season_data, label
