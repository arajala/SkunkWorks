# SkunkWorks: An open-source NCAA football ranking and prediction program.
# Copyright (C) 2021  Arthur Rajala

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Add the root package directory to path for importing
# This is so user does not need to run setup.py or modify PYTHONPATH
from os.path import dirname, join, realpath
import sys
root = dirname(dirname(dirname(realpath(__file__))))
sys.path.append(join(root, "TheKickIsBAD"))

# Standard imports
import statistics
import the_kick_is_bad
from the_kick_is_bad import utils


def calculate_norms(start_year, end_year):

    points = []
    first_downs = []
    rushing_yards = []
    rushing_attempts = []
    passing_yards = []
    passing_attempts = []
    total_yards = []
    total_plays = []
    total_fumbles = []
    lost_fumbles = []
    penalties = []
    penalty_yards = []
    punts = []
    punt_yards = []
    punt_returns = []
    punt_return_yards = []
    kickoff_returns = []
    kickoff_return_yards = []
    interception_returns = []
    interception_return_yards = []
    third_down_convs = []
    third_down_atts = []
    fourth_down_convs = []
    fourth_down_atts = []

    # Loop over years to average statistics
    for year in range(start_year, end_year + 1):

        stats = the_kick_is_bad.read_stats(year, "bowl")

        for team in stats:

            points.append(statistics.mean(stats[team]["points"]["total"]["gained"]))
            first_downs.append(statistics.mean(stats[team]["1st downs"]["total"]["gained"]))
            rushing_yards.append(statistics.mean(stats[team]["rushing"]["yards"]["gained"]))
            rushing_attempts.append(statistics.mean(stats[team]["rushing"]["attempts"]["gained"]))
            passing_yards.append(statistics.mean(stats[team]["passing"]["yards"]["gained"]))
            passing_attempts.append(statistics.mean(stats[team]["passing"]["attempts"]["gained"]))
            total_yards.append(statistics.mean(stats[team]["total offense"]["yards"]["gained"]))
            total_plays.append(statistics.mean(stats[team]["total offense"]["plays"]["gained"]))
            total_fumbles.append(statistics.mean(stats[team]["fumbles"]["total"]["gained"]))
            lost_fumbles.append(statistics.mean(stats[team]["fumbles"]["lost"]["gained"]))
            penalties.append(statistics.mean(stats[team]["penalties"]["number"]["gained"]))
            penalty_yards.append(statistics.mean(stats[team]["penalties"]["yards"]["gained"]))
            punts.append(statistics.mean(stats[team]["punts"]["number"]["gained"]))
            punt_yards.append(statistics.mean(stats[team]["punts"]["yards"]["gained"]))
            punt_returns.append(statistics.mean(stats[team]["punt returns"]["number"]["gained"]))
            punt_return_yards.append(statistics.mean(stats[team]["punt returns"]["yards"]["gained"]))
            kickoff_returns.append(statistics.mean(stats[team]["kickoff returns"]["number"]["gained"]))
            kickoff_return_yards.append(statistics.mean(stats[team]["kickoff returns"]["yards"]["gained"]))
            interception_returns.append(statistics.mean(stats[team]["interception returns"]["number"]["gained"]))
            interception_return_yards.append(statistics.mean(stats[team]["interception returns"]["yards"]["gained"]))
            third_down_convs.append(statistics.mean(stats[team]["3rd downs"]["conversions"]["gained"]))
            third_down_atts.append(statistics.mean(stats[team]["3rd downs"]["attempts"]["gained"]))
            fourth_down_convs.append(statistics.mean(stats[team]["4th downs"]["conversions"]["gained"]))
            fourth_down_atts.append(statistics.mean(stats[team]["4th downs"]["attempts"]["gained"]))
            
    norms = {
        "points": (statistics.mean(points), statistics.stdev(points)),
        "first downs": (statistics.mean(first_downs), statistics.stdev(first_downs)),
        "rushing yards": (statistics.mean(rushing_yards), statistics.stdev(rushing_yards)),
        "rushing attempts": (statistics.mean(rushing_attempts), statistics.stdev(rushing_attempts)),
        "passing yards": (statistics.mean(passing_yards), statistics.stdev(passing_yards)),
        "passing attempts": (statistics.mean(passing_attempts), statistics.stdev(passing_attempts)),
        "total yards": (statistics.mean(total_yards), statistics.stdev(total_yards)),
        "total plays": (statistics.mean(total_plays), statistics.stdev(total_plays)),
        "total fumbles": (statistics.mean(total_fumbles), statistics.stdev(total_fumbles)),
        "lost fumbles": (statistics.mean(lost_fumbles), statistics.stdev(lost_fumbles)),
        "penalties": (statistics.mean(penalties), statistics.stdev(penalties)),
        "penalty yards": (statistics.mean(penalty_yards), statistics.stdev(penalty_yards)),
        "punts": (statistics.mean(punts), statistics.stdev(punts)),
        "punt yards": (statistics.mean(punt_yards), statistics.stdev(punt_yards)),
        "punt returns": (statistics.mean(punt_returns), statistics.stdev(punt_returns)),
        "punt return yards": (statistics.mean(punt_return_yards), statistics.stdev(punt_return_yards)),
        "kickoff returns": (statistics.mean(kickoff_returns), statistics.stdev(kickoff_returns)),
        "kickoff return yards": (statistics.mean(kickoff_return_yards), statistics.stdev(kickoff_return_yards)),
        "interception returns": (statistics.mean(interception_returns), statistics.stdev(interception_returns)),
        "interception return yards": (statistics.mean(interception_return_yards), statistics.stdev(interception_return_yards)),
        "third down convs": (statistics.mean(third_down_convs), statistics.stdev(third_down_convs)),
        "third down atts": (statistics.mean(third_down_atts), statistics.stdev(third_down_atts)),
        "fourth down convs": (statistics.mean(fourth_down_convs), statistics.stdev(fourth_down_convs)),
        "fourth down atts": (statistics.mean(fourth_down_atts), statistics.stdev(fourth_down_atts)),
    }

    # Norms to file
    absolute_path = utils.get_abs_path(__file__)
    filename = f"{absolute_path}/norms.json"
    utils.write_json(norms, filename)


if __name__ == "__main__":
    start_year = int(sys.argv[1])
    end_year = int(sys.argv[2])
    calculate_norms(start_year, end_year)
