# SkunkWorks: An open-source NCAA football ranking and prediction program.
# Copyright (C) 2019  Arthur Rajala

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Add the root package directory to path for importing
# This is so user does not need to run setup.py or modify PYTHONPATH
from os.path import dirname, join, realpath
import sys
root = dirname(dirname(dirname(realpath(__file__))))
sys.path.append(join(root, "TheKickIsBAD"))

# Standard imports
import json
import the_kick_is_bad
from the_kick_is_bad import utils


def read_results(year, week):

    # Check if the week is 'bowl' week
    num_weeks = the_kick_is_bad.read_number_of_weeks(year)
    week, _ = utils.check_week(week, num_weeks)

    # Open results file with absolute path
    absolute_path = utils.get_abs_path(__file__)
    filename = f"{absolute_path}/{year}/results-{year}-{week:02}.csv"
    with open(filename, "r") as file:

        results = []

        # Remove the header line
        _ = file.readline().strip()

        # Loop through lines to read prediction data
        result_line = file.readline().strip()
        while result_line:

            # Split the prediction data
            result = result_line.split(",")
            away_team = result[0]
            home_team = result[1]
            predicted_winner = result[2]
            result_string = result[3]
            prediction_confidence = result[4]
            margin_of_victory = result[5]

            # Pack prediction structure
            results.append({
                "away team": away_team,
                "home team": home_team,
                "predicted winner": predicted_winner,
                "result": result_string,
                "prediction confidence": float(prediction_confidence),
                "margin of victory": float(margin_of_victory)
            })

            result_line = file.readline().strip()

        return results

if __name__ == "__main__":
    year = int(sys.argv[1])
    week = sys.argv[2]
    if week != "bowl":
        week = int(week)
    results = read_results(year, week)
    results_string = json.dumps(results, indent=2)
    print(results_string)
