# SkunkWorks: An open-source NCAA football ranking and prediction program.
# Copyright (C) 2019  Arthur Rajala

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Add the root package directory to path for importing
# This is so user does not need to run setup.py or modify PYTHONPATH
from os.path import dirname, join, realpath
import sys
root = dirname(dirname(dirname(realpath(__file__))))
sys.path.append(join(root, "TheKickIsBAD"))

# Standard imports
import the_kick_is_bad
from the_kick_is_bad import utils


def read_predictions(year, week):

    # Check if the week is 'bowl' week
    num_weeks = the_kick_is_bad.read_number_of_weeks(year)
    week, _ = utils.check_week(week, num_weeks)

    # Open predictions file with absolute path
    absolute_path = utils.get_abs_path(__file__)
    filename = f"{absolute_path}/{year}/predictions-{year}-{week:02}.csv"
    with open(filename, "r") as file:

        predictions = []

        # Remove the header line
        _ = file.readline().strip()

        # Loop through lines to read prediction data
        prediction_line = file.readline().strip()
        while prediction_line:

            # Split the prediction data
            prediction = prediction_line.split(",")
            away_team = prediction[0]
            home_team = prediction[1]
            predicted_winner = prediction[2]
            prediction_confidence = prediction[3]

            # Pack prediction structure
            predictions.append({
                "away team": away_team,
                "home team": home_team,
                "predicted winner": predicted_winner,
                "prediction confidence": float(prediction_confidence)
            })

            prediction_line = file.readline().strip()

        return predictions
