# SkunkWorks

SkunkWorks is an NCAA football prediction application, built on the data platform TheKickIsBAD. It uses machine learning models on statistics since 2013 to predict the winner of games, rank all teams, conferences, and divsions, and evaluate past predictions on a weekly basis.

The models are built in Pytorch, which is captured in the environment.yml file, along with all other dependencies.
